---
weight: 4
title: "iWine REST API методы"
---

# iWine REST API методы

## GET /version {.get}

Показывает версию программного обеспечения, модель и mac-адрес устройства.

**Параметры вызова**:

 <table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
    optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
</table> 

**Ответ**: Статус **200 Ok**

```json
{
    "version": <string>,
    "model": <string>
    "mac": <mac-address>
}
```
**Параметры ответа**:
 <table class="method_params">
  <tr>
    <td>version</td>
    <td>Версия программного обеспечения</td>
  </tr>
  <tr>
    <td>model</td>
    <td>Модель iWine</td>
  </tr>
  <tr>
    <td>mac</td>
    <td>MAC-адрес изделия</td>
  </tr>
</table> 
<br />

## GET /volume {.get}

Показывает имеющийся объём вина в графине, либо незаполненный объём.

**Параметры вызова**:

 <table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
    optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
  <tr>
    <td>units</td>
    <td>optional</td>
    <td>Единицы измерения объёма. <br />
    Возможные значения: default, ml, inch3.<br />
    default - настройки по умолчанию,<br />
    ml - миллилитры,<br />
    inch3 - кубические дюймы. <br />
    По умолчанию: default.</td>
  </tr>
  <tr>
    <td>reverse</td>
    <td>optional</td>
    <td>Если true, показывает объём недостающей жидкости до максимума. <br />
    Возможные значения: true, false. По умолчанию: false.</td>
  </tr>
</table> 

**Ответ**: Статус **200 Ok**

```json
{
    "volume": <float number>,
    "reverse": <boolean>
    "units": <string>
}
```
**Параметры ответа**:
 <table class="method_params">
  <tr>
    <td>volume</td>
    <td>Объём жидкости, либо оставшийся до максимума объём жидкости.</td>
  </tr>
  <tr>
    <td>reverse</td>
    <td>False, если указан объём имеющейся жидкости (по умолчанию). True, если указан пустой объём.</td>
  </tr>
  <tr>
    <td>units</td>
    <td>Единицы измерения объёма. 
    Возможные значения: ml, inch3.<br />
  </td>
  </tr>
</table> 
<br />


## GET /alcohol {.get}

Показывает содержание алкоголя в напитке.

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
  <tr>
    <td>units</td>
    <td>optional</td>
    <td>Единицы измерения измерения алкоголя. <br />
    Возможные значения: default, ABV, degree, proof.<br />
    default - настройки по умолчанию,<br />
    ABV - % ABV (Alcohol by volume), процент от объёма,<br />
    degree - градусы, концентрация спирта в массе (Россия), <br />
    proof - proof spirit, ABV*2 (США), <br />
    По умолчанию: default.</td>
  </tr>
</table>

**Ответ**: Статус **200 Ok**

```json
{
    "alcohol": <float number>,
    "units": <string>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>alcohol</td>
    <td>Количество спирта в указанных единицах измерения</td>
  </tr>
    <tr>
    <td>units</td>
    <td>Единицы измерения содержания спирта.<br />
    Возможные значения: ABV, degree, proof.<br />

  </tr>
</table>


## GET /sugar {.get}

Показывает содержание сахара в напитке.

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
  <tr>
    <td>units</td>
    <td>optional</td>
    <td>Единицы измерения измерения алкоголя.<br />
    Возможные значения: default, gpl, Blg.<br />
    default - настройки по умолчанию,<br />
    gpl - gram per liter, грамм на литр,<br />
    Blg - °Balling, шкала Боллинга<br />
    Bx - °Bx, шкала Брикса <br />
    По умолчанию: default.</td>
  </tr>
</table>

**Ответ**: Статус **200 Ok**

```json
{
    "sugar": <float number>,
    "units": <string>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>sugar</td>
    <td>Количество сахара в указанных единицах измерения</td>
  </tr>
    <tr>
    <td>units</td>
    <td>Единицы измерения содержания спирта.<br />
    Возможные значения: gpl, Blg, Bx.<br />
  </tr>
</table>
<br />

## GET /temperature.get {.get}

Показывает температуру напитка на момент запроса.

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
  <tr>
    <td>units</td>
    <td>optional</td>
    <td>Единицы измерения температуры.<br />
    Возможные значения: default, C, F.<br />
    default - настройки по умолчанию,<br />
    C - °Celsius, градус Цельсия,<br />
    F - °Fahrenheit, градус по Фаренгейту<br />
    По умолчанию: default.</td>
  </tr>
</table>

**Ответ**: Статус **200 Ok**

```json
{
    "temperature": <float number>,
    "units": <string>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>temperature</td>
    <td>Температура в указанных единицах измерения</td>
  </tr>
    <tr>
    <td>units</td>
    <td>Единицы измерения температуры.<br />
    Возможные значения: Celsius, Fahrenheit.<br />
  </tr>
</table>
<br />

## GET /type_of_wine {.get}

Показывает тип вина: красное, белое, розовое, оранжевое.

<div class="attention">

:heavy_exclamation_mark: Внимание! Эта информация определяется с использованием алгоритмов машинного обучения и имеет неполную достоверность определения.
</div>

**Параметры вызова**:

 <table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
    optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
</table> 

**Ответ**: Статус **200 Ok**

```json
{
    "type_of_wine": <string>,
    "confidence": <integer>
}
```
**Параметры ответа**:
 <table class="method_params">
  <tr>
    <td>type_of_wine</td>
    <td>Тип вина</td>
  </tr>
  <tr>
    <td>confidence</td>
    <td>Достоверность определения результатов, %</td>
  </tr>
</table> 
<br />

## POST /temperature.set {.post}

Устанавливает температуру напитка. Возможен как нагрев, так и охлаждение в зависимости от изначальной температуры.

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required</td>
    <td>Токен с правами на изменение.</td>
  </tr>
  <tr>
    <td>temperature</td>
    <td>required</td>
    <td>Температура до которой нужно охладить или нагреть напиток. Integer или float number.</td>
  </tr>
  <tr>
    <td>units</td>
    <td>optional</td>
    <td>Единицы измерения температуры.<br />
    Возможные значения: default, C, F.<br />
    default - настройки по умолчанию,<br />
    C - °Celsius, градус Цельсия,<br />
    F - °Fahrenheit, градус по Фаренгейту<br />
    По умолчанию: default.</td>
  </tr>
  <tr>
    <td>async</td>
    <td>optional</td>
    <td>Если true - всегда получать ответ со статусом 202 Accepted. False по умолчанию.</td>
  </tr>
</table>

<div class="attention">

:heavy_exclamation_mark: Время доведения до нужной температуры не мгновенно и зависит от текущего состояния напитка. Метод асинхронный, полученный ответ имеет статус **202 Accepted** и содержит идентификатор асинхронной функции, по которому с помощью метода ***async*** можно определить завершённость процесса. Если вино на момент вызова метода уже нужной температуры придёт ответ со статусом  **200 Ok**.
</div>


**Ответ**: Статус **200 Ok**

```json
{
    "status": "finished"
    "temperature": <float number>,
    "units": <string>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>status</td>
    <td>Статус завершения операции.</td>
  </tr>
  <tr>
    <td>temperature</td>
    <td>Температура в указанных единицах измерения</td>
  </tr>
    <tr>
    <td>units</td>
    <td>Единицы измерения.<br />
    Возможные значения: Celsius, Fahrenheit.<br />
  </tr>
</table>
<br />

ИЛИ 

**Ответ**: Статус **202 Accepted**

```json
{
    "status": <"in-progress" or "finished">,
    "async_id": <string>,
    "estimated_time": <integer>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>status</td>
    <td>Статус завершения операции.</td>
  </tr>
    <tr>
    <td>async_id</td>
    <td>Идентификатор асинхронного метода.
  </tr>
    </tr>
  <tr>
    <td>estimated_time</td>
    <td>Расчётное время завершения операции, секунды.
</table>
<br />

## POST /shake {.post}

Взбалтывает вино для насыщения его воздухом.

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required</td>
    <td>Токен с правами на изменение.</td>
  </tr>
</table>

<div class="attention">

:heavy_exclamation_mark: Время взбалтывания составляет 5 минут (300 секунд). Метод асинхронный, полученный ответ имеет статус **202 Accepted** и содержит идентификатор асинхронной функции, по которому с помощью метода ***async*** можно определить завершённость процесса. 
</div>

**Ответ**: Статус **202 Accepted**

```json
{
    "status": "in-progress",
    "async_id": <string>,
    "time_to_complete": <integer>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>status</td>
    <td>Статус завершения операции.</td>
  </tr>
  <tr>
    <td>async_id</td>
    <td>Идентификатор асинхронного метода.
  </tr>
    </tr>
  <tr>
    <td>time_to_complete</td>
    <td>Время до завершения операции, секунды.
</table>
<br />

## GET /async {.get}

Показывает статус и прогресс длительной операции по идентификатору метода поддерживающего асинхронность. 

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
  <tr>
    <td>async_id</td>
    <td>required</td>
    <td>Идентификатор асинхронного метода</td>
  </tr>
</table>

**Ответ**: Статус **200 Ok**

```json
{
    "async_id": <string>,
    "method": <string>,
    "params": {"параметры метода": "значения параметров"},
    "status": <"in-progress" or "finished">,
    "progress": <integer>
}
```

**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>async_id</td>
    <td>Идентификатор асинхронного метода.
  </tr>
  <tr>
    <td>method</td>
    <td>Название метода</td>
  </tr>
  <tr>
    <td>params</td>
    <td>Группа. Параметры метода.</td>
  </tr> 
  <tr>
    <td>status</td>
    <td>Статус выполнения операции.</td>
  </tr> 
  <tr>
    <td>progress</td>
    <td>Прогресс в процентах.</td>
  </tr> 
</table>
<br />

## GET /settings.get {.get}

Показывает настройки.

**Параметры вызова**:
<table class="method_params">
  <tr>
    <td>token</td>
    <td>required*<br />
optional</td>
    <td>Токен с правами на чтение. Обязателен если не разрешены анонимные подключения.</td>
  </tr>
</table>

**Ответ**: Статус **200 Ok**

```json
{
"language": <string>,
"max_volume": <string>,
"units": 
  {
    "volume": <string>,
    "alcohol": <string>,
    "sugar": <string>,
    "temperature": <string> 
  }
}
```
**Параметры ответа**:
<table class="answer_params">
  <tr>
    <td>language</td>
    <td>Код языка: en, ru.</td>
  </tr>

  
  <tr>
    <td>units</td>
    <td>Группа. Единицы измерения по умолчанию:</td>
  </tr>

  <tr>
    <td>> volume</td>
    <td>Единицы измерения объёма.<br />
      Возможные значения: ml, inch3.<br />
      ml - миллилитры,<br />
      inch3 - кубические дюймы. <br />
      По умолчанию: ml.</td>
    </tr>
  <tr>
    <td>> alcohol</td>
    <td>Единицы измерения содержания алкоголя.<br />
      Возможные значения: ABV, degree, proof.<br />
      ABV - % ABV (Alcohol by volume), процент от объёма,<br />
      degree - градусы, концентрация спирта в массе (Россия),<br />
      proof - proof spirit, ABV*2 (США), <br />
      По умолчанию: ABV.</td>
  </tr>
  <tr>
    <td>> sugar</td>
    <td>Единицы измерения содержания сахара.<br />
      Возможные значения: gpl, Blg.<br />
      gpl - gram per liter, грамм на литр,<br />
      Blg - °Balling, шкала Боллинга<br />
      Bx - °Bx, шкала Брикса <br />
      По умолчанию: gpl.</td>
  </tr>
  <tr>
    <td>> temperature</td>
    <td>Единицы измерения содержания температуры.<br />
      Возможные значения: C, F.<br />
      C - °Celsius, градус Цельсия,<br />
      F - °Fahrenheit, градус по Фаренгейту<br />
      По умолчанию: C.</td>
  </tr>
  </table>
  <br />

## POST /settings.set {.post}

Устанавливает настройки настройки.

**Параметры вызова**:

```json
{
"token": <string>,
"language": <string>,
"units": 
  {
    "volume": <string>,
    "alcohol": <string>,
    "sugar": <string>,
    "temperature": <string> 
  }
}
```

<table class="method_params">
  <tr>
    <td>token</td>
    <td>required</td>
    <td>Токен с правами на изменение настроек.</td>
  </tr>

  <tr>
    <td>language</td>
    <td>optional</td>
    <td>Код языка: en, ru.</td>
  </tr>

  
  <tr>
    <td>units</td>
    <td>optional</td>
    <td>Группа.</td>
  </tr>

  <tr>
    <td>> volume</td>
    <td>optional</td>
    <td>Единицы измерения объёма.<br />
      Возможные значения: ml, inch3.<br />
      ml - миллилитры,<br />
      inch3 - кубические дюймы.
    </tr>
  <tr>
    <td>> alcohol</td>
    <td>optional</td>
    <td>Единицы измерения содержания алкоголя.<br />
      Возможные значения: ABV, degree, proof.<br />
      ABV - % ABV (Alcohol by volume), процент от объёма,<br />
      degree - градусы, концентрация спирта в массе (Россия),<br />
      proof - proof spirit, ABV*2 (США)
  </tr>
  <tr>
    <td>> sugar</td>
    <td>optional</td>
    <td>Единицы измерения содержания сахара.<br />
      Возможные значения: gpl, Blg.<br />
      gpl - gram per liter, грамм на литр,<br />
      Blg - °Balling, шкала Боллинга<br />
      Bx - °Bx, шкала Брикса
  </tr>
  <tr>
    <td>> temperature</td>
    <td>optional</td>
    <td>Единицы измерения содержания температуры.<br />
      Возможные значения: C, F.<br />
      C - °Celsius, градус Цельсия,<br />
      F - °Fahrenheit, градус по Фаренгейту
  </tr>
</table>

**Ответ**: Статус **200 Ok**

Ответ идентичен ***settings.get***.
Параметры которые не были указаны остаются с предыдущими значениями.
<br />