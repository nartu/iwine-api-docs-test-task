---
weight: 3
title: "Как использовать iWine REST API"
---

# Как использовать iWine REST API

## Запросы

Запросы могут быть GET и POST методов. Ответ приходит в формате json.

Пример GET запроса:

```
curl -X GET --header 'Accept: application/json' http://IP/METHOD?arg1=value1&arg2=value2
```

 Пример POST запроса:

 ```
 curl -d '{"key1":"value1", "key2":"value2"}' -H "Content-Type: application/json" -X POST http://IP/METHOD
 ```

В случае успеха приходит ответ со статусом `200 Ok` и телом ответа:
```
{
    "key1": "value1",
    "key2": "value2"
}
```
В случае неуспешного выполнения метода приходит ответ со статусом `4XX Some Error` или `5XX Some Error` и телом ответа, в котором содержится уникальный текстовый код ошибки и её описание:
```json
{
    "error": "UniqueTextCodeError",
    "description": "Описание ошибки"
}
```
<br />

### Асинхронные операции

Некоторые методы требуют время на исполнение и операции выполняются асинхронно. Эти методы обозначаются словом ***async***. В случае успешного принятия запроса приходит ответ со статусом `202 Accepted` и телом:
```json
{
    "status": "in-progress",
    "async_id": <идентификатор асинхронной операции>: String,
    "estimated_time": <расчётное время завершения в секундах>: Integer
}
```

После чего по уникальному идентификатору *async_id* асинхронной операции можно узнать статус её завершения:
```
curl -X GET --header 'Accept: application/json' http://IP/async?async_id=<async_id>
```
В случае успеха приходит ответ со статусом `200 Ok` и телом ответа:
```json
{
    "async_id": <идентификатор асинхронного метода>: String,
    "method": <название метода>: String,
    "params": {"параметры метода": "значения параметров"},
    "status": "in-progress" или "finished",
    "progress": <процент>: Integer
}
```
