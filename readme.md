That's the [test task](https://drive.google.com/drive/u/0/folders/1HOSgYW2Gt51wOmTuG4YUlmsFbBPdbs43) for vacancy Software Documentation Developer в [documentat.io](https://documentat.io/)

## Deploy

https://iwine-docs.onrender.com


## Сборка

Во всех вариантах результат смотреть по адресу: http://localhost:1313

### 1 вариант

Зависимости: docker, docker-compose (для старых версий)
1. Загрузить [docker-compose.yaml](https://gitlab.com/nartu/iwine-api-docs-test-task/-/raw/main/compose.yaml) из этого репозитория
2. `docker compose up`
3. Для завершения `docker compose down`

### 2 вариант

Зависимости: docker

1. Запустить контейнер с hugo-extended в режиме командной строки
```
docker run --rm -it \
  klakegg/hugo:0.101.0-ext-alpine \
  shell
```

2. Внутри контейнера ввести команду
```
apk add --update --no-cache go git && \
hugo mod init github.com/gohugoio/myShortcodes && \
wget https://gitlab.com/nartu/iwine-api-docs-test-task/-/raw/main/config.toml && \
hugo server
```
3. Для завершения `exit` в командной строке контейнера

### 3 вариант

Зависимости: hugo-extended, go, git, linux shell
1. Там где есть разрешение на запись ввести команду
```
mkdir test-task && cd test-task \
hugo mod init github.com/gohugoio/myShortcodes && \
wget https://gitlab.com/nartu/iwine-api-docs-test-task/-/raw/main/config.toml && \
hugo server
```
2. Команда `rm -R test-task` стирает созданную директорию

